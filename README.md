- Markdown guide https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/markdown.md
- ```sdk tools version 25.2.5 < 26.1.1``` hoac ```sdk tools version 0 < 26.1.1```
  dung unity hub de download android sdk ve thay vi su dung sdk down o ngoai
- ```FAILURE: Build failed with an exception Error while generating the main dex list unity```

   - ```Deprecated Gradle features were used in this build, making it incompatible with Gradle 6.0```.

     vào mainTemplate.gradle sửa version của com.android.tools.build:gradle sang version cao hơn (tham khảo https://forum.unity.com/threads/com-android-build-api-transform-transformexception-error-while-generating-the-main-dex-list.723794/)
ngoài ra có thể google 'gradle compatibility' để biết đc library bản nào thì tương thích với gradle 6., để biet android gradle plugin nao tuong thich voi gradle ban nao, xem tai https://developer.android.com/studio/releases/gradle-plugin, de biet unity ban nao dung plugin gradle ban nao, xem tai https://docs.unity3d.com/Manual/android-gradle-overview.html
   - ```duplicate class buildconfig found in modules```

     `Duplicate class com.android.installreferrer.BuildConfig found in modules classes.jar (:com.android.installreferrer.installreferrer-1.1:) and classes.jar (:installreferrer-1.0:)`

     do trong library của Fb sdk có class bị trùng với class trong lib khác, giải pháp là chọn phiên bản lib của fb sdk sao cho ko có class này bằng cách mở file Assets/FacebookSDK/Plugins/Editor/Dependencies.xml sửa ```[5,6)``` thành ```5.13.0``` trong đoạn
     ```xml
     <androidPackage spec="com.facebook.android:facebook-core:[5,6)" />
     <androidPackage spec="com.facebook.android:facebook-applinks:[5,6)" />
     <androidPackage spec="com.facebook.android:facebook-login:[5,6)" />
     <androidPackage spec="com.facebook.android:facebook-share:[5,6)" />
     ```
     tham khảo https://forum.unity.com/threads/android-unity-cloud-build-started-failing-build-today-solved-fb-sdk-issue.813891/
   - ```Try: Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.```

     để có được thêm thông tin thì chạy lại lệnh build gradle với option --stacktrace bằng cách:
      1. copy lại lệnh mà unity đã thực hiện (tìm trong log của unity editor, nội dung sẽ kiểu như 
         >C:\Program Files\Unity-2018.3.0f2\Editor\Data\PlaybackEngines\AndroidPlayer/Tools\OpenJDK\Windows\bin\java.exe -classpath "C:\Program Files\Unity-2018.3.0f2\Editor\Data\PlaybackEngines\AndroidPlayer\Tools\gradle\lib\gradle-launcher-4.6.jar" org.gradle.launcher.GradleMain "-Dorg.gradle.jvmargs=-Xmx4096m" "assembleRelease"
      2. mở commandline và cd vào thư mục Temp\gradleOut nằm trong thư mục của project
      3. chạy lệnh vừa copy ở trên cộng thêm option --stacktrace
      4. có stacktrace rồi, ngồi soi thôi

     tham khảo https://stackoverflow.com/questions/51008545/unity3d-how-to-enable-stacktrace
   - ```Duplicate class android.support.customtabs.ICustomTabsCallback found in modules classes.jar (androidx.core:core:1.0.1) and classes.jar (com.android.support:customtabs:26.1.0)```
     mở Display libraries cua Play Services Resolver thi thay doan
     ```xml
     implementation 'com.android.support:customtabs:25.3.1' // Facebook.Unity.Editor.AndroidSupportLibraryResolver.addSupportLibraryDependency
     ```
     chung to rang facebook su dung library nay va bi conflict voi phien ban Android SDK Build tool dang su dung. 2 giai phap: 1 la thay the facebook, 2 la thay doi phien ban Android SDK Build tool
   - ```The number of method references in a .dex file cannot exceed 64K``` mặc dù đã enable multidex : kiểm tra xem minSdkVersion co >= 21 hay ko, neu ko thi sua lai, tham khao https://forum.unity.com/threads/cant-build-with-multidex-enabled.773348/ va https://developer.android.com/studio/build/multidex
   - PLACE_HOLDER

- `FAILURE: Build failed with an exception.`
    ```markdown
    * What went wrong:
    Execution failed for task ':packageRelease'.
    > Failed to obtain compression information for entry
    ```
    hoặc
    ```markdown
    * What went wrong:
    Execution failed for task ':packageRelease'.
    > Execution of compression failed.
    ```
    hoặc
    ```markdown
    * What went wrong:
    Could not dispatch a message to the daemon.
    ```
    Tăng virtual memory hoặc RAM

    Bật custom gradle của Unity và thêm đoạn sau vào file gradle `mainTemplate.gradle`
    ```markdown
    android {
      dexOptions {
        javaMaxHeapSize "4g"
      }
    }
    ```

- `FAILURE: Build failed with an exception.`
    ```log
    * What went wrong:
    Execution failed for task ':launcher:transformNativeLibsWithMergeJniLibsForRelease'.
    > More than one file was found with OS independent path 'lib/x86/libFirebaseCppAuth.so'
    ```
    Bật custom gradle của Unity và thêm đoạn sau vào Assets/Plugins/Android/launcherTemplate.gradle
    ```log
    android {
      packagingOptions {
        pickFirst '**/*.so'
      }
    }
    ````

- Việc add thêm library vào project có thể thực hiện qua 2 cách: download thủ công file library (trên https://mvnrepository.com) hoặc khai báo 1 file dependencies.xml để PlayServiceResolver nó tự động down hộ. Quy tắc khai báo tham khảo ở https://github.com/googlesamples/unity-jar-resolver đoạn 
    ##### Android Resolver Usage

    The Android Resolver copies specified dependencies from local or remote Maven repositories into the Unity project when a user selects Android as the build target in the Unity editor.

   1. Add the `play-services-resolver-*.unitypackage` to your plugin
      project (assuming you are developing a plugin). If you are redistributing
      the Play Services Resolver with your plugin, you **must** follow the
      import steps in the [Getting Started](#getting-started) section!

   2. Copy and rename the `SampleDependencies.xml` file into your
      plugin and add the dependencies your plugin requires.

      The XML file just needs to be under an `Editor` directory and match the
      name `*Dependencies.xml`. For example,
      `MyPlugin/Editor/MyPluginDependencies.xml`.

   3. Follow the steps in the [Getting Started](#getting-started)
      section when you are exporting your plugin package.

  For example, to add the Google Play Games library
(`com.google.android.gms:play-services-games` package) at version `9.8.0` to
the set of a plugin's Android dependencies:

  ```xml
  <dependencies>
    <androidPackages>
      <androidPackage spec="com.google.android.gms:play-services-games:9.8.0">
        <androidSdkPackageIds>
          <androidSdkPackageId>extra-google-m2repository</androidSdkPackageId>
        </androidSdkPackageIds>
      </androidPackage>
    </androidPackages>
  </dependencies>
  ```

  The version specification (last component) supports:

     * Specific versions e.g `9.8.0`
     * Partial matches e.g `9.8.+` would match 9.8.0, 9.8.1 etc. choosing the most
     recent version.
     * Latest version using `LATEST` or `+`.  We do *not* recommend using this
     unless you're 100% sure the library you depend upon will not break your
     Unity plugin in future.

  The above example specifies the dependency as a component of the Android SDK manager such that the Android SDK manager will be executed to install the package if it's not found.  If your Android dependency is located on Maven
central it's possible to specify the package simply using the `androidPackage` element:

  ```xml
  <dependencies>
    <androidPackages>
      <androidPackage spec="com.google.api-client:google-api-client-android:1.22.0" />
    </androidPackages>
  </dependencies>
  ```
- ```FAILURE: Unable to convert classes to dex format ( After import plugin UnityAds )```
  - Lỗi xảy ra khi tích hợp thêm mediation `UnityAds` cho `Ironsrc`, nguyên nhân vì `Unity đã tích hợp sẵn UnityAds` -> `duplicate library`
  - Để sửa lỗi, cần xóa bỏ UnityAds đã được tích hợp sẵn
  - Mở Unity -> vào `Window\Package Manager`, trong đó có package `Advertisment` đã được installl, click vào nhấn `Remove`



- ```FAILURE: Duplicate classes from androidx and com.android.support ```
  - Lỗi xảy ra khi tích hợp plugin sử dụng library androidx và com.android.support
  - => mở `Build Setting` -> `Play Setting` -> sử dụng `Custom Gradle Properties Template`
  - Mở file `gradleTemplate` trong Unity -> thêm property
    `android.useAndroidX=true
      android.enableJetifier=true`


- ```FAILURE: Execution failed for task ':launcher:lintVitalRelease'.```
                  > Lint infrastructure error
                  Caused by: java.lang.reflect.InvocationTargetException
  - Lỗi xảy ra do enable lintoptions checkReleaseBuilds
  - => mở `Build Setting` -> `Play Setting` -> sử dụng `Custom Gradle Properties Template`
  - Mở file `gradleTemplate` trong Unity ->
    ``` xml 
    android {
      lintOptions {
          checkReleaseBuilds false
          // Or, if you prefer, you can continue to check for errors in release builds,
          // but continue the build even when errors are found:
          abortOnError false
          }
    }
    ```

- ```FAILURE: Build failed with an exception.```
  ```xml
  What went wrong:
  Could not determine the dependencies of task ':launcher:preReleaseBuild'.
    Could not resolve all task dependencies for configuration ':launcher:releaseRuntimeClasspath'.
      Could not find com.google.firebase:firebase-analytics-unity:6.4.0.
        Searched in the following locations:
          - https://dl.google.com/dl/android/maven2/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - https://jcenter.bintray.com/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - file:/C:/Users/Admin/TeamCity/buildAgent/work/383559699f722218/SSAR3/Temp/gradleOut/unityLibrary/libs/firebase-analytics-unity-6.4.0.jar
          - file:/C:/Users/Admin/TeamCity/buildAgent/work/383559699f722218/SSAR3/Temp/gradleOut/unityLibrary/libs/firebase-analytics-unity.jar
          - https://maven.google.com/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - file:/C:/Users/Admin/TeamCity/buildAgent/work/383559699f722218/SSAR3/Assets/Firebase/m2repository/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - file:/C:/Users/Admin/TeamCity/buildAgent/work/383559699f722218/SSAR3/Assets/GooglePlayGames/Editor/m2repository/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - https://dl.bintray.com/ironsource-mobile/android-sdk/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - https://dl.bintray.com/ironsource-mobile/android-adapters/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - file:/C:/Users/Admin/.m2/repository/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
          - https://repo.maven.apache.org/maven2/com/google/firebase/firebase-analytics-unity/6.4.0/firebase-analytics-unity-6.4.0.pom
        Required by:
            project :launcher > project :unityLibrary
  ```

  Lỗi xảy ra khi trong mainTemplate.gradle chứa khai báo dependencies với library có phiên bản bị cũ (6.4.0) so với phiên bản mới hơn khai báo trong file *Dependencies.xml. Khi chạy External Dependency Manager thì file mainTemplate.gradle đc append config của dependencies thay vì replace nên bị lưu lại config cũ. Sửa lại = tay, xóa những config cũ đi là được.

- ```Lỗi bị cache asset trên máy build ```
  Lỗi do Unity bị cache asset trên máy build => Reimport những asset bị lỗi

- sau khi cài build lên máy thấy texture bị sọc
  là do tool compress texture bị lỗi khi xử lý parallel
  https://forum.unity.com/threads/failed-etc2-compression.392092/ - [archive](archive/FailedETC2Compression-UnityForum(1_24_2022_9_43_22_AM).html)
  cách xử lý: build 1 tool wrapper rồi đè lên tool gốc , sau đó import lại hết asset rồi build lại game
  ```
  Instructions:

  Close all instances of a Unity 3d Editor

  Find installation path for a PVRTexTool in unity3d editor. Default "/opt/Unity/Editor/Data/Tools"
  Backup command file "/opt/Unity/Editor/Data/Tools/PVRTexTool"

  Copy or rename original file PVRTexTool to "PVRTexToolCLI" (or similar if you change the path in source code)

  Remember backup original file
  Copy compiled program to "/opt/Unity/Editor/Data/Tools/PVRTexTool" (same name)

  Golang compiler v 1.7
  ```
  tool này build từ source ở link trên
  cách build: cài go sdk (bản portable cũng oke), copy root folder của sdk vào C:\, tạo folder có tên là PVRTexTool , copy code vào file có tên là PVRTexTool.go , chạy lệnh `go build`

# IOS

- Ios resolver lỗi khi cài đặt CocoaPods:
  ```xml
  gem install activesupport -v 4.2.6 --user-install
  WARNING:  You don't have /Users/mac/.gem/ruby/2.6.0/bin in your PATH,
      gem executables will not run.Successfully installed thread_safe-0.3.6
  Successfully installed tzinfo-1.2.9
  Building native extensions. This could take a while...
  ERROR:  Error installing activesupport:
	ERROR: Failed to build gem native extension.
  ```
  xử lý:

    bước 1: cài ruby bản khác, ko sử dụng bản ruby builtin của macos

    https://stackoverflow.com/a/54873916
    
    ```
    brew install ruby
    echo 'export PATH="/usr/local/opt/ruby/bin:$PATH"' >> ~/.zshrc
    ````
    kiểm tra xem đã dùng đúng ruby vừa cài (thay vì dùng của os)
    ```
    which ruby
    ```

    bước 2: cài cocoa pods thủ công

    https://forum.unity.com/threads/failed-to-install-cocoapods.489700/#post-5822362
    
    ```
    cd unity/project/root/folder
    gem install cocoapods
    ```

    nếu vẫn gặp lỗi:

    ```
    gem install activesupport -v 4.2.6 --user-install
    WARNING:  You don't have /Users/mac/.gem/ruby/2.6.0/bin in your PATH,
        gem executables will not run.Building native extensions. This could take a while...
    ERROR:  Error installing activesupport:
      ERROR: Failed to build gem native extension.
    ```
      
    xử lý: 

    https://stackoverflow.com/a/61658158

    ```
    brew cleanup -d -v
    brew install cocoapods
    ```
- ko thấy file .xcworkspace

    - https://forum.unity.com/threads/how-to-get-unity-to-build-an-xcode-workspace-or-xcworkspace-file.797922/
      
      @TreyK-47 Thanks! Assets -> Play Services Resolver -> iOS Resolver -> Settings and then setting Cocoapods to Generate an xcworkspace worked! 

    - một khả năng khác là do conflict giữa dependencies của các library với nhau, khiến cho cocoaPods ko chạy đc nên ko generate đc file .xcworkspace. Kiểm tra bằng cách bật terminal, vào thư mục root của project xcode, chạy cocoaPods thủ công:
      ```
      pod install
      ```
      sẽ thấy thông báo lỗi, ví dụ:
      ```
      [!] CocoaPods could not find compatible versions for pod "GoogleAppMeasurement":
        In Podfile:
          Firebase/Analytics (= 7.0.0) was resolved to 7.0.0, which depends on
            Firebase/Core (= 7.0.0) was resolved to 7.0.0, which depends on
              FirebaseAnalytics (= 7.0.0) was resolved to 7.0.0, which depends on
                GoogleAppMeasurement (= 7.0.0)

          IronSourceAdMobAdapter (= 4.3.11.1) was resolved to 4.3.11.1, which depends on
            Google-Mobile-Ads-SDK (= 7.59.0) was resolved to 7.59.0, which depends on
              GoogleAppMeasurement (~> 6.0)
      ```
      xử lý: upgrade lib mà có dependency thấp, hoặc downgrade lib có dependency cao bằng cách edit phiên bản của lib trong file Podfile. Sau đó chạy lại lệnh `pod install`, nếu thành công sẽ có file .xcworkspace

    - Khi chạy `pod install` mà gặp lỗi `curl (92) http/2 stream 0 was not closed cleanly internal_error` thì thử đổi kết nối mạng sang 4G.
- Crash khi startup
    ```
    2021-01-27 09:30:51.719384+0700 SSAR3DEV[8975:5595002] [DYMTLInitPlatform] platform initialization successful
    2021-01-27 09:30:52.078665+0700 SSAR3DEV[8975:5594777] Error loading /var/containers/Bundle/Application/79C6D7E4-37D1-433E-B7D7-66372B9452A1/SSAR3DEV.app/Frameworks/UnityFramework.framework/UnityFramework:  
      dlopen(/var/containers/Bundle/Application/79C6D7E4-37D1-433E-B7D7-66372B9452A1/SSAR3DEV.app/Frameworks/UnityFramework.framework/UnityFramework, 265): Library not loaded: @rpath/libswiftCore.dylib
      Referenced from: /private/var/containers/Bundle/Application/79C6D7E4-37D1-433E-B7D7-66372B9452A1/SSAR3DEV.app/Frameworks/FBSDKCoreKit.framework/FBSDKCoreKit
      Reason: image not found
    ```
    xử lý:

    https://stackoverflow.com/a/26949219

    ```
    For me none of the previous solutions worked. We discovered that there is an "Always Embed Swift Standard Libraries" flag in the Build Settings that needs to be set to YES. It was NO by default!

    Build Settings > Always Embed Swift Standard Libraries

    After setting this, clean the project before building again.
    ```
- Xcode báo lỗi ở mục Signing & Capabilities > Signing > Status

  `Provisioning profile "ShadowHunterFree_AppStoreDistribution" doesn't include signing certificate "iPhone Developer: Anh Nguyen Tu (2TG3YT7YXP)".`

  xử lý:

  https://stackoverflow.com/a/52055056

  ```
  2) you specified the signing certificate in the Build Setting->Signing, 
  so go to the Build Setting->Signing and click the Code Signing Identity. 
  Do not select Automatic (iOS Developer or iOS Distribution), 
  select the one of the signing certificates in the Identities in Keychain which is valid and have associated with the Provisioning Profile. 
  ```
- Xcode khi Archive:
  ```
  /Users/mac/Downloads/projects/ssar3_output_build_ios_free/ssar3/Unity-iPhone.xcodeproj 
  The linked library 'InControlNative.a' is missing one or more architectures required by this target: armv7.
  ```
  xử lý: đổi architecture của project sang arm64

  Compatibility matrix: https://developer.apple.com/library/archive/documentation/DeviceInformation/Reference/iOSDeviceCompatibility/DeviceCompatibilityMatrix/DeviceCompatibilityMatrix.html

- Lỗi firebase khi init:
  `Either make sure googleservice-info.plist is included in your build or specify option`
  Add file GoogleService-Info.plist vào target trên Xcode bằng cách modify file Xcode khi build Unity
  Add thêm static method
  ```
  [PostProcessBuild(999)]
		public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
		{
			if (buildTarget == BuildTarget.iOS)
			{
				ModifyFrameworks(path);
			}
		}
 
		private static void ModifyFrameworks(string path)
		{
			string projPath = PBXProject.GetPBXProjectPath(path);
           
			var project = new PBXProject();
			project.ReadFromFile(projPath);
 
			string mainTargetGuid = project.GetUnityMainTargetGuid();
			string googleInfoPlistGuid = project.FindFileGuidByProjectPath("GoogleService-Info.plist");
			project.AddFileToBuild(mainTargetGuid, googleInfoPlistGuid);
			project.WriteToFile(projPath);
         }

  ```

  - Lỗi decimal point trên các region (VN ....)
    Add thêm static method
    ```
            [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		static void FixCulture()
		{
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		}
    ```
  - Lỗi app bị freeze khi click vào Facebook ( Hiện webview trắng xóa xong back về app và bị freeze)
  ```
    Vào Player Setting trên Unity => IOS Setting => Disable RenderExtraFrameOnPause
  ```



